# Your AI for CTF must inherit from the base Commander class.  See how this is
# implemented by looking at the commander.py in the ./api/ folder.
from api import Commander

# The commander can send 'Commands' to individual bots.  These are listed and
# documented in commands.py from the ./api/ folder also.
from api import commands

# The maps for CTF are layed out along the X and Z axis in space, but can be
# effectively be considered 2D.
from api import Vector2

from math import *

from random import random, choice
class HadrianCommander(Commander):
    """
    Rename and modify this class to create your own commander and add mycmd.Placeholder
    to the execution command you use to run the competition.
    """

    def initialize(self):
        """Use this function to setup your bot before the game starts."""
        
        # Calculate flag positions and store the middle.
        ours = self.game.team.flag.position
        theirs = self.game.enemyTeam.flag.position
        self.middle = (theirs + ours) / 2.0

        # Now figure out the flaking directions, assumed perpendicular.
        d = (ours - theirs)
        self.left = Vector2(-d.y, d.x).normalized()
        self.right = Vector2(d.y, -d.x).normalized()
        self.front = Vector2(d.x, d.y).normalized()
        self.mode = "attack";
        self.prevMode = "attack";
        self.verbose = True    # display the command descriptions next to the bot labels
        
        self.previousAliveBots = self.game.team.members
        self.previousEvents = []
        
        nBots = len(self.game.team.members)
        nObjBots = ceil(nBots/2)+1
        self.squad = {"objective":[],"patrol":[]}
        for i,bot in enumerate(self.game.team.members):
            if len(self.squad["objective"])<nObjBots:
                self.squad["objective"].append(bot)
                bot.squad = "objective"
            else:
                bot.squad = "patrol"
                self.squad["patrol"].append(bot)
        
        self.attackingBots = []
        self.gameId = choice(range(0,1000))
   

    def getFlankingPosition(self, bot, target):
        try:
            flanks = [target + f * 16.0 for f in [self.left, self.right]]
            options = map(lambda f: self.level.findNearestFreePosition(f), flanks)
            return sorted(options, key = lambda p: (bot.position - p).length())[0]   
        except:
            return target
    
    def patrolCenter(self, bot):
        
        box = min(self.level.width, self.level.height)
        target = self.level.findRandomFreePositionInBox((self.game.team.flag.position + box * 0.2, self.game.team.flag.position - box * 0.2))
        #sightTarget = (target + self.game.team.flag.position) / 2
        
        # issue the order
        if target:
            self.issue(commands.Attack, bot, target,  description = 'Patrolling Center')
        return
        
    def attackEnemyFlag(self, bot):
        target = self.game.enemyTeam.flag.position
        flank = self.getFlankingPosition(bot, target)
        if((bot.position - target).length() < self.level.firingDistance): #get the flag
            self.issue(commands.Attack, bot, target, description = 'get flag', lookAt=target)
        elif (target - flank).length() > (bot.position - target).length():
            #close to flag
            sightTarget = self.level.findRandomFreePositionInBox((target, bot.position))
            self.issue(commands.Attack, bot, target, description = 'attack flag from flank', lookAt=target)
        else:
            #closer to flank than flag
            flank = self.level.findNearestFreePosition(flank)
            self.issue(commands.Move, bot, flank, description = 'running to flank')
        return
    
    def getClosestKnownEnemy(self, bot):
        targets = self.game.enemyTeam.members
        targets = filter(lambda x: x.health > 0 and x.position!=None ,targets)
        try:
            if targets:
                if(len(targets)==1):
                    target = targets[0]
                else:
                    # sort by distance
                    target = sorted(targets,key=lambda t: (t.position-bot.position).length())[0]
                    return target.position
        except:
            pass
        return self.game.team.flag.position
    
    def defendFlagCarrier(self, bot):
        enemyAlive = filter(lambda x: x.health > 0 and x.position!=None ,self.game.enemyTeam.members)
        if(bot.flag):
            target = self.game.team.flagScoreLocation
            #closest known enemy target
            if enemyAlive:
                self.issue(commands.Attack, bot, target, description = 'Fighting to home', lookAt = target)
            else:
                self.issue(commands.Charge, bot, target, description = 'Charging home')

        else:
            if enemyAlive:
                target = self.getClosestKnownEnemy(bot)
                self.issue(commands.Attack, bot, target, description = 'Defending our flag carrier', lookAt = target)
            else:
                box = self.level.botSpawnAreas[self.game.enemyTeam.name]
                enemySpawn = (box[0]-box[1])/2
                box = self.level.botSpawnAreas[self.game.team.name]
                spawn = (box[0]-box[1])/2
                
                if((enemySpawn - bot.position).length() < (spawn - bot.position).length()):
                    box = min(self.level.width, self.level.height)
                    target = self.level.findRandomFreePositionInBox((enemySpawn + box * 0.1, enemySpawn - box * 0.1))
                    self.issue(commands.Attack,target, bot, description = 'Camping enemy spawn', facingDirection = enemySpawn)
                else:
                    box = min(self.level.width, self.level.height)
                    target = self.level.findRandomFreePositionInBox((spawn + box * 0.1, spawn - box * 0.1))
                    self.issue(commands.Charge,bot, target, description = 'Regrouping')

        return
    
    def pursueOurFlag(self, bot):
        target = self.game.team.flag.position
        self.issue(commands.Charge, bot, target, description = 'Pursuing our flag')
        return

    
    def tick(self):
        """Override this function for your own bots.  Here you can access all the information in self.game,
        which includes game information, and self.level which includes information about the level."""
		
        # half the bots follow this, half patrol the center
        #if we're attacking, maybe we should be defending:
        # no one has the flag -> attack
        # we have the flag, they don't -> protect flag bearer and score zone
        # they have the flag, we don't -> flank their scoring position, pursue our flag
        # Both teams have the flag -> protect flag bearer and score zone
        
        # keep state of our bots, keep squads balanced.
        
        eventNames = ("TYPE_NONE", "TYPE_KILLED", "TYPE_FLAG_PICKEDUP", "TYPE_FLAG_DROPPED", "TYPE_FLAG_CAPTURED")
        
        def printEvent(event):
            def whichTeam(team):
                if team.name == self.game.team.name:
                    return "Us"
                else:
                    return "Them"
            status = "objBots "+str(len(filter(lambda x: x in self.game.bots_alive, self.squad["objective"])))+" "
            status+= "patrolBots "+str(len(filter(lambda x: x in self.game.bots_alive, self.squad["patrol"])))
            print str(self.gameId)+" : "+eventNames[event.type]+", "+whichTeam(event.instigator.team )+" -> "+whichTeam(event.subject.team)+" | "+status
        
        
        if len(self.game.match.combatEvents) > len(self.previousEvents):
            # new event!
            printEvent(self.game.match.combatEvents[-1])
            self.previousEvents = self.game.match.combatEvents
        
        theyHaveFlag = bool([x.name for x in self.game.enemyTeam.members if x.flag])
        weHaveFlag = bool([x.name for x in self.game.bots_alive if x.flag])
        
        for bot in self.squad["objective"]:
            if bot in filter(lambda x: not (x in self.attackingBots or x.flag),self.game.bots_alive): #overrides
                targets = list(set(bot.seenBy + bot.visibleEnemies))
                targets = filter(lambda x: x.health > 0,targets)
                if targets:
                    
                    if(len(targets)==1):
                        target = targets[0]
                    else:
                        # sort by distance
                        target = sorted(targets,key=lambda t: (t.position-bot.position).length())[0]
                        #target2 = self.getFlankingPosition(bot, target.position)
                        #target = 
                    if weHaveFlag:
                        dist = self.level.firingDistance*1.4
                    else:
                        dist = self.level.firingDistance*2.5
                    if (bot.position-target.position).length() < dist: 
                        self.issue(commands.Attack, bot, target.position, description = 'Pursuing seen enemy')
                        self.attackingBots.append(bot)
                        continue
            if bot in self.game.bots_available: #don't spam commands!
                #no one has the flag:
                if not (theyHaveFlag or weHaveFlag):
                    self.attackEnemyFlag(bot)
                    
                if weHaveFlag and not theyHaveFlag:
                    self.defendFlagCarrier(bot)
                if theyHaveFlag and not weHaveFlag:
                    self.attackEnemyFlag(bot)
                if weHaveFlag and theyHaveFlag:
                    self.defendFlagCarrier(bot)
                if bot in self.attackingBots: self.attackingBots.remove(bot)

                    
        for bot in self.squad["patrol"]:
            if bot in filter(lambda x: not (x in self.attackingBots or x.flag),self.game.bots_alive): #overrides
                targets = list(set(bot.seenBy + bot.visibleEnemies))
                targets = filter(lambda x: x.health > 0,targets)
                if targets:
                    
                    if(len(targets)==1):
                        target = targets[0]
                    else:
                        # sort by distance
                        target = sorted(targets,key=lambda t: (t.position-bot.position).length())[0]
                    #target = self.getFlankingPosition(bot, target.position)
                    self.issue(commands.Attack, bot, target.position, description = 'Pursuing seen enemy')
                    self.attackingBots.append(bot)
                    continue
            if bot in self.game.bots_available: #don't spam commands!
                # pick a random position in the level to move to
                if theyHaveFlag and not weHaveFlag:
                    self.pursueOurFlag(bot)      
                else:
                    self.patrolCenter(bot)
                if bot in self.attackingBots: self.attackingBots.remove(bot)
            #override
            
            
        
        # # for all bots which aren't currently doing anything
        # for bot in self.game.bots_available:
            # if mode=="defend":
                # # if a bot has the flag run to the scoring location
                # if bot.flag: 
                    # flagScoreLocation = self.game.team.flagScoreLocation
                    # self.issue(commands.Attack, bot, flagScoreLocation, description = 'Run to my flag')
                # else:
                    # # defend the scoring position
                    # flagScoreLocation = self.game.team.flagScoreLocation
                    # self.issue(commands.Defend, bot, flagScoreLocation, description = 'Defend our flag')
            # else:
                # # otherwise run to where the flag is
                # target = self.game.enemyTeam.flag.position
                # flank = self.getFlankingPosition(bot, target)
                # if (target - flank).length() > (bot.position - target).length():
                    # self.issue(commands.Charge, bot, target, description = 'attack from flank')
                # else:
                    # flank = self.level.findNearestFreePosition(flank)
                    # self.issue(commands.Move, bot, flank, description = 'running to flank')


    def shutdown(self):
        """Use this function to teardown your bot after the game is over, or perform an
        analysis of the data accumulated during the game."""

        pass
